#include <stdlib.h>
#include "internal_representation.h"
#include "bmp_utils.h"

static const char* read_status_message[] = {
        [READ_OK] = "The image was successfully read",
        [TYPE_ERR] = "The image has incorrect type",
        [RESERVED_ERR] = "The image is not correct",
        [UNSUPPORTED_FORMAT] = "Format of this image is unsupported",
        [READ_ERR] = "Error during reading image"
};

static const char* write_status_message[] = {
        [WRITE_OK] = "The image was written into the new file",
        [WRITE_ERROR] = "There was an error during saving the image"
};

const char* get_read_status_message(enum read_status err_code) {
    return read_status_message[err_code];
}

const char* get_write_status_message(enum write_status err_code) {
    return write_status_message[err_code];
}

size_t calculate_padding(uint64_t img_width) {
    size_t padding = 4 - (img_width * 3) % 4;
    return padding == 4 ? 0 : padding;
}

struct bmp_header create_header_to_image(const struct image img, uint16_t bfType) {
    return (struct bmp_header) {
            bfType,
            (img.width + calculate_padding(img.width)) * img.height + B_OFF_BITS_DEFAULT,
            BF_RESERVED_DEFAULT,
            B_OFF_BITS_DEFAULT,
            BI_SIZE_DEFAULT,
            img.width,
            img.height,
            BI_PLANES_DEFAULT,
            BI_BIT_COUNT_DEFAULT,
            BI_COMPRESSION_DEFAULT,
            BI_SIZE_IMAGE_DEFAULT,
            BI_X_PELS_PER_METER_DEFAULT,
            BI_Y_PELS_PER_METER_DEFAULT,
            BI_CLR_USED_DEFAULT,
            BI_CLR_IMPORTANT_DEFAULT
    };
}

enum read_status from_bmp(FILE* in, struct image* const img, struct bmp_header* const header) {
    int8_t tr[4] = {0};
    int32_t row;
    const size_t pad = calculate_padding(img->width);;
    fread(header, sizeof(struct bmp_header), 1, in);
    if (header->bfType != BMP_BIG_ENDIAN && header->bfType != BMP_LITTLE_ENDIAN) {
        return TYPE_ERR;
    }
    if (header->bfReserved != 0) {
        return RESERVED_ERR;
    }
    if (header->biBitCount != 24) {
        return UNSUPPORTED_FORMAT;
    }
    img->width = header->biWidth;
    img->height = header->biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    fseek(in, header->bOffBits, SEEK_SET);

    for (row = 0; row < img->height; row++) {
        if(!fread(&img->data[row*img->width], sizeof(struct pixel), img->width, in)) {
            return READ_ERR;
        }
        if (pad != 0) {
            fread(tr, sizeof(int8_t), pad, in);
        }
    }
    return READ_OK;
}

enum write_status write_bmp(FILE* out, const struct image img, const struct bmp_header header) {
    const int8_t tr[4] = {0};
    int32_t row;
    const size_t pad = calculate_padding(img.width);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (row = 0; row < img.height; row++) {
        if (!fwrite(&img.data[row*img.width], sizeof(struct pixel), img.width, out)) {
            return WRITE_ERROR;
        }
        if (pad != 0) {
            fwrite(tr, sizeof(int8_t), pad, out);
        }
    }
    return WRITE_OK;
}
