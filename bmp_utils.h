#ifndef LAB5_BMP_UTILS_H
#define LAB5_BMP_UTILS_H

#include <stdint.h>
#include <stdio.h>

#define BMP_BIG_ENDIAN 0x424D
#define BMP_LITTLE_ENDIAN 0x4D42

#define BF_RESERVED_DEFAULT 0
#define B_OFF_BITS_DEFAULT 54
#define BI_SIZE_DEFAULT 40
#define BI_PLANES_DEFAULT 1
#define BI_BIT_COUNT_DEFAULT 24
#define BI_COMPRESSION_DEFAULT 0
#define BI_SIZE_IMAGE_DEFAULT 0
#define BI_X_PELS_PER_METER_DEFAULT 0
#define BI_Y_PELS_PER_METER_DEFAULT 0
#define BI_CLR_USED_DEFAULT 0
#define BI_CLR_IMPORTANT_DEFAULT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK = 0,
    TYPE_ERR,
    RESERVED_ERR,
    UNSUPPORTED_FORMAT,
    READ_ERR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* read, struct bmp_header* header);
enum write_status write_bmp(FILE* out, struct image write, struct bmp_header header);
struct bmp_header create_header_to_image(struct image img, uint16_t bfType);

const char* get_read_status_message(enum read_status);
const char* get_write_status_message(enum write_status);

#endif //LAB5_BMP_UTILS_H
