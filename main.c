#include <stdio.h>
#include "internal_representation.h"
#include "image_transformator.h"
#include "bmp_utils.h"

int main() {
    FILE* in, *out;
    struct bmp_header source_header;
    struct image source_img;
    struct bmp_header new_header;
    struct image new_img;
    enum read_status read_err_code;
    enum write_status write_err_code;


    in = fopen("../lights.bmp", "rb");
    out = fopen("../out.bmp", "wb");

    read_err_code = from_bmp(in, &source_img, &source_header);
    printf("%s\n", get_read_status_message(read_err_code));
    new_img = rotate(source_img);
    new_header = create_header_to_image(new_img, source_header.bfType);

    write_err_code = write_bmp(out, new_img, new_header);
    printf("%s\n", get_write_status_message(write_err_code));

    fclose(in);
    fclose(out);
    return 0;
}
