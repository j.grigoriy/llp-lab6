//
// Created by Григорий Жаворонков on 17.11.2020.
//

#ifndef LAB5_INTERNAL_REPRESENTATION_H
#define LAB5_INTERNAL_REPRESENTATION_H

struct __attribute__((packed))pixel {
    unsigned char b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //LAB5_INTERNAL_REPRESENTATION_H
