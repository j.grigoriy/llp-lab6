#ifndef LAB5_IMAGE_TRANSFORMATOR_H
#define LAB5_IMAGE_TRANSFORMATOR_H

#include <stdint.h>
#include <stdio.h>
#include "internal_representation.h"
#include "bmp_utils.h"

struct image rotate(struct image img);

#endif //LAB5_IMAGE_TRANSFORMATOR_H
