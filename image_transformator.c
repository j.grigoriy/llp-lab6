#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "image_transformator.h"

static void translate(double* tr_x, double* tr_y, double x, double y, double angle) {
    *tr_x = x * cos(angle) - y * sin(angle);
    *tr_y = x * sin(angle) + y * cos(angle);
}

struct image rotate(const struct image img) {
    double angle = -90 * M_PI / 180;
    double offset_x = img.width / 2;
    double offset_y = img.height / 2;
    double new_offset_x = offset_y;
    double new_offset_y = offset_x;

    struct image new_img = {
            img.height,
            img.width,
            malloc(new_img.height * new_img.width * sizeof(struct pixel))
    };

    for (uint32_t y = 0; y < new_img.height; y++) {
        for (uint32_t x = 0; x < new_img.width; x++) {
            double tr_x, tr_y;
            int32_t new_x, new_y;
            translate(&tr_x, &tr_y, x - new_offset_x, y - new_offset_y, angle);
            new_x = round(tr_x + offset_x);
            new_y = round(tr_y + offset_y);
            new_img.data[y * new_img.width + x] = img.data[new_y * img.width + new_x];
        }
    }

    return new_img;
}

